import re
from datetime import datetime

logfile = "sample.log"
with open(logfile) as log:
    log = log.readlines()
currentdate = datetime.now()
for line in log:
    timestamp = line[:19]
    date_time_obj = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
    date = currentdate - date_time_obj
    if date.days <= 1:
        if re.search("ERROR", line):
            print(f"{line}")
